'use strict';

/**
 * parametre-general router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::parametre-general.parametre-general');
