'use strict';

/**
 * parametre-general controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::parametre-general.parametre-general');
