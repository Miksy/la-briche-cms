'use strict';

/**
 * parametre-general service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::parametre-general.parametre-general');
