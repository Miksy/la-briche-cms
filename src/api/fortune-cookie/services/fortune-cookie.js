'use strict';

/**
 * fortune-cookie service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::fortune-cookie.fortune-cookie');
