'use strict';

/**
 * fortune-cookie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::fortune-cookie.fortune-cookie');
