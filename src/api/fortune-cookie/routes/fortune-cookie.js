'use strict';

/**
 * fortune-cookie router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::fortune-cookie.fortune-cookie');
