'use strict';

/**
 * brichou controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::brichou.brichou');
