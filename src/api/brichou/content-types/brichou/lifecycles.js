module.exports = {
  beforeUpdate(event) {
    event.params.data.contactable = event.params.data.email && event.params.data.email.length
  },
}
