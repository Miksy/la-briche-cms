'use strict';

/**
 * brichou service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::brichou.brichou');
