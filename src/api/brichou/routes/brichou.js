'use strict';

/**
 * brichou router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::brichou.brichou');
