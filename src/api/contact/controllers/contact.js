'use strict'
/**
 * contact controller
 */

const { createCoreController } = require('@strapi/strapi').factories

const isValidEmail = (email) => {
    var re = /\S+@\S+\.\S+/
    return re.test(email)
}

module.exports = createCoreController('api::contact.contact', ({ strapi }) => ({
    async create(ctx) {
        // some logic here
        const sanitizedData = await this.sanitizeInput(ctx.request.body.data)

        if (!ctx.request.body.data.brichoux?.length && !ctx.request.body.data.attractions?.length)
            return ctx.badRequest("Vous n'avez renseigné aucun destinataire")
        if (ctx.request.body.data.brichoux?.length > 10 || ctx.request.body.data.attractions?.length > 10)
            return ctx.badRequest("Trop de destinataires, merci de restreindre l'envoi à 10 destinataires")
        if (!isValidEmail(sanitizedData.email)) return ctx.badRequest('Merci de renseigner une adresse email valide')
        if (!sanitizedData.objet.length) return ctx.badRequest("Merci d'indiquer l'objet de votre message")
        if (sanitizedData.message.length < 50) return ctx.badRequest('Votre message doit comporter au moins 50 caractères')

        // create a new contact
        const response = await super.create(ctx)

        // get the id of the new contact
        const contactId = response.data.id

        // get the new contact object with all the info
        const entry = await strapi.entityService.findOne('api::contact.contact', contactId, {
            fields: ['id', 'email', 'objet', 'message'],
            populate: { attractions: true, brichoux: true },
        })

        if (entry.brichoux.length < 1 && entry.attractions.length < 1)
            return ctx.badRequest("L'erreur 525 est survenue, merci de reporter l'erreur sur l'email présent dans la page Infos")

        // get all the emails from the brichoux
        const brichoux_emails = entry.brichoux.map((a) => a.email)
        const brichoux_names = entry.brichoux.map((a) => a.prenom + ' ' + a.nom)

        const attractions_emails = entry.attractions.map((a) => a.email)
        const attractions_names = entry.attractions.map((a) => a.nom)

        const dest_emails = [...brichoux_emails, ...attractions_emails]
        const dest_names = [...brichoux_names, ...attractions_names].join(', ')

        const message = sanitizedData.message.replaceAll('\n', '<br>')

        //send mails
        const emailResponse = await strapi.plugin('email').services.email.send({
            to: dest_emails,
            from: 'ministere@labriche.fr',
            subject: 'Annuaire Briche : ' + sanitizedData.objet,
            html: `Vous avez reçu un message de la part de ${sanitizedData.email} :<br>
          <b>objet</b><br>
          ${sanitizedData.objet}<br>
          <br>
          <b>message</b><br>
          ${message}`,
        })

        const emailConfirmation = await strapi.plugin('email').services.email.send({
            to: sanitizedData.email,
            from: 'ministere@labriche.fr',
            subject: 'Annuaire Briche : ' + sanitizedData.objet,
            html: `Votre message a bien été envoyé à ${dest_names}<br>
          <br>
          <b>objet</b><br>
          ${sanitizedData.objet}<br>
          <br>
          <b>message</b><br>
          ${message}`,
        })

        return response
    },
}))
